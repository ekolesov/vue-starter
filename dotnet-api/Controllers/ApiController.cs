﻿using Microsoft.AspNetCore.Mvc;
using VueStarter.Api.ViewModels;

namespace VueStarter.Api.Controllers
{
  [Route("v1")]
  [ApiController]
  public class ApiController : ControllerBase
  {
    [HttpGet("doc")]
    public IActionResult Doc(string key)
    {
      var result = new DocViewModel(){
        Key = key,
        Title = "Doc from dotnet api",
        Text = "Doc content for key " + key
      };
      return Ok(result);
    }
  }
}
