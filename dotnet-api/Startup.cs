using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.Globalization;
using Newtonsoft.Json;

namespace VueStarter.Api
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
      services
        .AddControllers()
        .AddNewtonsoftJson(options =>
        {
          options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
          options.SerializerSettings.Converters.Add
              (new Newtonsoft.Json.Converters.StringEnumConverter());

          options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        });

      services.AddCors();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      var cultureInfo = new CultureInfo("ru-RU");
      CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
      CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;

      app.UseRouting();

      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        app.UseCors(x => x
          .WithOrigins("https://localhost:5000")
          .AllowAnyHeader()
          .AllowAnyMethod());
      }
      else
      {
        app.UseCors(x => x
          .WithOrigins("https://demo.rlib.ru")
          .AllowAnyHeader()
          .AllowAnyMethod());
      }

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });

      app.Run(async (context) =>
      {
        await context.Response.WriteAsync("api");
      });
    }
  }
}
