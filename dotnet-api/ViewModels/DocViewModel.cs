using Newtonsoft.Json;

namespace VueStarter.Api.ViewModels {
  public class DocViewModel
  {
    [JsonProperty(PropertyName = "key")]
    public string Key { get; set; }

    [JsonProperty(PropertyName = "title")]
    public string Title { get; set; }

    [JsonProperty(PropertyName = "text")]
    public string Text { get; set; }
  }
}