## Vue starter node api

Заглушка api с https и cors

### Установка
``` yarn ```

### Запуск
``` yarn rundev ```

### SSL в режиме разработки

Сертификат и приватный ключ должны находиться в папке ` parts/ssl `

Про сертификат localhost на винде для node можно почитать здесь:
https://blog.kodono.info/wordpress/2018/08/23/create-a-self-signed-certificate-for-localhost-testing-with-ie11-and-webpack/

для остального:
https://github.com/webpack/webpack-dev-server/tree/master/examples/cli/https

#### Хост
https://localhost:5010

dotnet-api настроен на этот же порт, поэтому запускать нужно только один из проектов
