const express = require('express')
const cors = require('cors')
const fs = require('fs')
const https = require('https')

const app = express()
const privateKey = fs.readFileSync('./parts/ssl/localhost.pem', 'utf8')
const certificate = fs.readFileSync('./parts/ssl/localhost.crt', 'utf8')
const credentials = { key: privateKey, cert: certificate }

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

var corsOptions = {
  origin: 'https://localhost:5000',
  optionsSuccessStatus: 200,
}

app.use(cors(corsOptions))

app.get('/', function (req, res) {
  res.send("api")
})

app.get('/v1/doc', function (req, res) {
  let docKey = req.query.key

  var result = {
    key: docKey,
    title: 'Doc from node api',
    text: 'Doc content for key ' + docKey,
  }
  res.send(result)
})

const httpsServer = https.createServer(credentials, app)
httpsServer.listen(5010)
