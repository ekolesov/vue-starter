## Vue starter

Может быть запущен как на базе веб-сервера dotnet, так и только в окружении node.

Демо: [https://demo.rlib.ru/](https://demo.rlib.ru/)


### vue-app

Стартер для приложения vue. Используется vuex, ssr, router, meta, vuelidate, v-mask, pug, stylus. Подробнее в readme проекта.

### dotnet-api

Заглушка для api на базе dotnet

### node-api

Заглушка для api на базе express