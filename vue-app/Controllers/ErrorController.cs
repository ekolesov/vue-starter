﻿using Microsoft.AspNetCore.Mvc;

namespace VueStarter.App
{
  public class ErrorController : ControllerBase
  {
    [HttpGet("/error")]
    public IActionResult Error(int? statusCode = null)
    {
      var err = "404";
      if (statusCode.HasValue)
      {
        HttpContext.Response.StatusCode = statusCode.Value;
        err = statusCode.Value.ToString();
      }

      return File("~/handler/error/error" + err + ".html", "text/html");
    }
  }
}