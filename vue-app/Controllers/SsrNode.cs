﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.NodeServices;

namespace VueStarter.App
{
  public class SsrNodeController : Controller
  {
    private readonly INodeServices _nodeServices;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public SsrNodeController(INodeServices nodeServices, IHttpContextAccessor httpContextAccessor)
    {
      _nodeServices = nodeServices;
      _httpContextAccessor = httpContextAccessor;
    }

    public async Task<IActionResult> Index()
    {
      var url = _httpContextAccessor.HttpContext.Request.Path.Value;
      try
      {
        var r = await _nodeServices.InvokeAsync<RenderResult>("./parts/server/dotnet", new { url = url });

        if (r.Status == 200)
        {
          return Content(r.Html, "text/html");
        }
        else
        {
          return BadRequest();
        }
      }
      catch
      {
        return NotFound();
      }
    }
  }

  public class RenderResult
  {
    public string Html { get; set; }
    public string Err { get; set; }

    public int Status { get; set; }
  }
}