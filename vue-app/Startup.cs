using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Rewrite;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using System;

namespace VueStarter.App
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddControllers();
      services.AddHttpContextAccessor();
      services.AddNodeServices();

      // Enable the Gzip compression especially for Kestrel
      services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Optimal);
      services.AddResponseCompression(options =>
      {
        options.EnableForHttps = true;
      });
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseStatusCodePagesWithReExecute("/error", "?statusCode={0}");
        app.UseHsts();
      }

      var options = new RewriteOptions()
        .Add(RedirectMultipleSlashes)
        .Add(RedirectToLowerCase);

      if (!env.IsDevelopment())
      {
        options.AddIISUrlRewrite(env.ContentRootFileProvider, "urlrewrite.xml");
      }

      app.UseRewriter(options);
      // app.UseDefaultFiles();

      app.UseHttpsRedirection();

      app.UseStaticFiles(new StaticFileOptions
      {
        // static cache
        OnPrepareResponse = ctx =>
        {
          const int durationInSeconds = 60 * 60 * 24 * 365;
          ctx.Context.Response.Headers.Add(HeaderNames.CacheControl, new[] { "public,max-age=" + durationInSeconds });
          ctx.Context.Response.Headers.Add(HeaderNames.Expires, new[] { DateTime.UtcNow.AddYears(1).ToString("R") }); // Format RFC1123
        }
      });

      app.UseResponseCompression(); // No need if you use IIS, but really something good for Kestrel!

      app.UseRouting();
      app.UseEndpoints(endpoints =>
      {
        endpoints.MapFallbackToController("Index", "SsrNode");
      });
    }

    static void RedirectMultipleSlashes(RewriteContext context)
    {
      var request = context.HttpContext.Request;
      var response = context.HttpContext.Response;
      var pathValue = request.Path.Value;

      if (Regex.IsMatch(pathValue, @"\/{2,}"))
      {
        var queryString = request.QueryString;
        var newPath = Regex.Replace(pathValue, @"\/{2,}", "/") + queryString.Value;

        response.StatusCode = StatusCodes.Status301MovedPermanently;
        context.Result = RuleResult.EndResponse;
        response.Headers[HeaderNames.Location] = newPath;
      }
    }

    static void RedirectToLowerCase(RewriteContext context)
    {
      var request = context.HttpContext.Request;
      var response = context.HttpContext.Response;
      var pathValue = request.Path.Value;

      if (Regex.IsMatch(pathValue, @"[A-Z]"))
      {
        var queryString = request.QueryString;
        var newPath = pathValue.ToLower() + queryString.Value;

        response.StatusCode = StatusCodes.Status301MovedPermanently;
        context.Result = RuleResult.EndResponse;
        response.Headers[HeaderNames.Location] = newPath;
      }
    }
  }
}
