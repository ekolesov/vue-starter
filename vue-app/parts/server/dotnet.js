const fs = require('fs')

const { createBundleRenderer } = require('vue-server-renderer')
const serverBundle = require('../bundle/vue-ssr-server-bundle.json')
const clientManifest = require('../../wwwroot/vue-ssr-client-manifest.json')
const renderer = createBundleRenderer(serverBundle, {
  runInNewContext: false,
  template: fs.readFileSync('./parts/templates/server.template.html', 'utf-8'),
  clientManifest,
  inject: false
})

process.env.VUE_ENV = 'server'
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

module.exports = async (callback, context) => {
  var html = await renderer.renderToString(context).then(
    res => {
      return {
        html: res,
        status: context.code
      }
    },
    err => {
      return {
        html: err,
        status: err.code
      }
    }
  )

  callback(null, html)
}
