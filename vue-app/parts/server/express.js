const fs = require('fs')
const path = require('path')
const express = require('express')
const https = require('https')
const compression = require('compression')
const dotenv = require('dotenv')

const { createBundleRenderer } = require('vue-server-renderer')
const serverBundle = require('../bundle/vue-ssr-server-bundle.json')
const clientManifest = require('../../wwwroot/vue-ssr-client-manifest.json')
const renderer = createBundleRenderer(serverBundle, {
  runInNewContext: false,
  template: fs.readFileSync('./parts/templates/server.template.html', 'utf-8'),
  clientManifest,
  inject: false,
})

process.env.VUE_ENV = 'server'
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

const server = express()

server.use(compression())
server.use('/', express.static(path.join(__dirname, '../../wwwroot')))

server.get('*', (req, res) => {
  const context = { url: req.url }

  renderer.renderToString(context, (err, html) => {
    if (err) {
      if (err.code === 404) {
        res.status(404).end('Not found')
      } else {
        res.status(500).end('Internal Server Error')
      }
    }

    res.end(html)
  })
})

// need .env file (local.env as example)
dotenv.config()

if (process.env.NODE_ENV !== 'production') {
  if (process.env.SSL_MODE === 'on') {
    const privateKey = fs.readFileSync('./parts/ssl/localhost.pem', 'utf8')
    const certificate = fs.readFileSync('./parts/ssl/localhost.crt', 'utf8')

    const credentials = { key: privateKey, cert: certificate }
    const httpsServer = https.createServer(credentials, server)

    httpsServer.listen(process.env.PUB_PORT, process.env.PUB_HOST, function() {
      console.log(
        '%s on port %d in %s mode, ssl is %s',
        process.env.PUB_HOST,
        process.env.PUB_PORT,
        process.env.NODE_ENV,
        process.env.SSL_MODE
      )
    })
  } else {
    server.listen(process.env.PUB_PORT, process.env.PUB_HOST)
  }
} else {
  // SSL_MODE ignored
  server.listen(process.env.PUB_PORT, process.env.PUB_HOST, function() {
    console.log(
      '%s on port %d in %s mode, ssl is %s (now ignored)',
      process.env.PUB_HOST,
      process.env.PUB_PORT,
      process.env.NODE_ENV,
      process.env.SSL_MODE
    )
  })
}
