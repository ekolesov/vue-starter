import axios from 'axios'
import VueAxios from 'vue-axios'

export default (vue) => {
  vue.use(VueAxios, axios)

  vue.axios.defaults.baseURL = process.env.API_BASE
}
