let clearMeta = (value) => {
  return value.replace(/&(amp;)?shy;/g, '')
}

const defaultImage = '/assets/img/defpic.png'
const defaultTitle = 'Vue starter'

export default {
  methods: {
    metaData() {
      var metaTitle = this.meta.title
        ? clearMeta(this.meta.title)
        : defaultTitle
      var isShort = this.meta.isShort === true

      var metaRender = {
        title: metaTitle,
        htmlAttrs: {
          lang: 'ru',
        },
        meta: isShort
          ? []
          : [
              {
                vmid: 'og:type',
                property: 'og:type',
                content: 'website',
              },
              {
                vmid: 'og:title',
                property: 'og:title',
                content: metaTitle,
              },
              {
                vmid: 'mrc__share_title',
                name: 'mrc__share_title',
                content: metaTitle,
              },
            ],
      }

      if (this.meta.description) {
        var metaDescription = this.meta.description
          ? clearMeta(this.meta.description)
          : ''
        metaRender.meta.push({
          vmid: 'description',
          property: 'description',
          content: metaDescription,
        })

        if (!isShort) {
          metaRender.meta.push({
            vmid: 'mrc__share_description',
            name: 'mrc__share_description',
            content: metaDescription,
          })
          metaRender.meta.push({
            vmid: 'og:description',
            property: 'og:description',
            content: metaDescription,
          })
        }
      }

      if (this.meta.url) {
        metaRender.link = metaRender.link || []
        metaRender.link = [
          {
            vmid: 'canonical',
            rel: 'canonical',
            href: this.meta.url,
          },
        ]

        if (!isShort) {
          metaRender.meta.push({
            vmid: 'og:url',
            property: 'og:url',
            content: this.meta.url,
          })
        }
      }

      if (!isShort) {
        if (!this.meta.img) {
          this.meta.img = defaultImage
        }

        metaRender.link = metaRender.link || []
        metaRender.link.push({
          vmid: 'image_src',
          rel: 'image_src',
          href: this.meta.img,
        })

        metaRender.meta.push({
          vmid: 'og:image',
          property: 'og:image',
          content: this.meta.img,
        })
      }

      return metaRender
    },
  },
}
