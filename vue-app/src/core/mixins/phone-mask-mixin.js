const maskCodes = {
  code33: ['495', '499', '812', '843', '800'],
  code44ext: ['4822', '8482'],
  code44: [
    '3012',
    '3022',
    '3412',
    '3422',
    '3432',
    '3433',
    '3435',
    '3439',
    '3452',
    '3462',
    '3466',
    '3472',
    '3473',
    '3512',
    '3517',
    '3519',
    '3532',
    '3812',
    '3822',
    '3823',
    '3832',
    '3833',
    '3842',
    '3843',
    '3852',
    '3854',
    '3912',
    '3919',
    '3951',
    '3952',
    '3953',
    '4012',
    '4112',
    '4152',
    '4162',
    '4212',
    '4217',
    '4232',
    '4242',
    '4712',
    '4722',
    '4725',
    '4732',
    '4742',
    '4752',
    '4812',
    '4842',
    '4852',
    '4855',
    '4862',
    '4872',
    '4912',
    '4922',
    '4932',
    '4942',
    '4964',
    '4966',
    '4967',
    '8112',
    '8142',
    '8152',
    '8172',
    '8182',
    '8202',
    '8212',
    '8247',
    '8312',
    '8313',
    '8332',
    '8342',
    '8352',
    '8362',
    '8412',
    '8422',
    '8442',
    '8443',
    '8452',
    '8462',
    '8463',
    '8469',
    '8512',
    '8552',
    '8553',
    '8555',
    '8612',
    '8617',
    '8622',
    '8632',
    '8652',
    '8672',
    '8712',
    '8722',
  ],
  code52: ['33'],
  code53: [
    '301',
    '302',
    '341',
    '342',
    '343',
    '345',
    '346',
    '347',
    '349',
    '351',
    '352',
    '353',
    '381',
    '382',
    '383',
    '384',
    '385',
    '388',
    '390',
    '391',
    '401',
    '411',
    '413',
    '415',
    '416',
    '421',
    '423',
    '424',
    '471',
    '472',
    '473',
    '474',
    '475',
    '481',
    '483',
    '484',
    '485',
    '486',
    '487',
    '491',
    '492',
    '493',
    '494',
    '496',
    '811',
    '814',
    '815',
    '816',
    '817',
    '818',
    '821',
    '831',
    '833',
    '834',
    '835',
    '836',
    '841',
    '842',
    '844',
    '845',
    '846',
    '847',
    '851',
    '855',
    '861',
    '863',
    '865',
    '866',
    '867',
    '871',
    '872',
    '877',
    '878',
    '879',
  ],
  code54: [
    '8126',
    '8127',
    '8434',
    '8436',
    '8437',
    '4823',
    '4824',
    '4825',
    '4826',
    '4827',
  ],
  code55: [
    '35171',
    '35172',
    '41126',
    '42622',
    '42722',
    '84316',
    '34391',
    '34394',
    '34397',
    '34398',
  ],
  code65: ['30137'],
  code66: ['401532', '401532', '401533', '423630', '482323'],
  code75: ['38243'],
  code76: ['401564', '401560', '401566', '415445'],
  code77: ['4015126'],
  code88: ['41544513'],
}

export default {
  created() {
    this.reMask = this.maskWatcher
  },

  methods: {
    maskWatcher() {
      var clearValue = this.phone.replace(/[^\d]/g, '')
      var reInit = false
      var newMask = null

      var prefix = clearValue.substring(0, 2)
      var startDigit = prefix == 88 || prefix == 89 ? 1 : 0
      var start8 = false
      var reInit8 = false
      var len = clearValue.length
      if (startDigit == 0 && len >= 2) {
        startDigit = clearValue.substring(0, 1) == 7 ? 1 : 0
        start8 = clearValue.substring(0, 1) == 8
      }

      if (len >= 8 + startDigit && !reInit) {
        if (
          maskCodes.code88.indexOf(
            clearValue.substring(startDigit, 8 + startDigit)
          ) >= 0
        ) {
          newMask = '(####-####) ##'
          reInit = true
        }
        if (startDigit == 0 && start8 && len >= 9 && !reInit) {
          if (maskCodes.code88.indexOf(clearValue.substring(1, 9)) >= 0) {
            newMask = '(####-####) ##'
            reInit = reInit8 = true
          }
        }
      }

      if (len >= 7 + startDigit && !reInit) {
        if (
          maskCodes.code77.indexOf(
            clearValue.substring(startDigit, 7 + startDigit)
          ) >= 0
        ) {
          newMask = '(###-####) #-##'
          reInit = true
        }
        if (startDigit == 0 && start8 && len >= 8 && !reInit) {
          if (maskCodes.code77.indexOf(clearValue.substring(1, 8)) >= 0) {
            newMask = '(###-####) #-##'
            reInit = reInit8 = true
          }
        }
      }

      if (len >= 6 + startDigit && !reInit) {
        if (
          maskCodes.code76.indexOf(
            clearValue.substring(startDigit, 6 + startDigit)
          ) >= 0
        ) {
          newMask = '(###-####) #-##'
          reInit = true
        }
        if (startDigit == 0 && start8 && len >= 7 && !reInit) {
          if (maskCodes.code76.indexOf(clearValue.substring(1, 7)) >= 0) {
            newMask = '(###-####) #-##'
            reInit = reInit8 = true
          }
        }
      }

      if (len >= 6 + startDigit && !reInit) {
        if (
          maskCodes.code66.indexOf(
            clearValue.substring(startDigit, 6 + startDigit)
          ) >= 0
        ) {
          newMask = '(###-###) ##-##'
          reInit = true
        }
        if (startDigit == 0 && start8 && len >= 7 && !reInit) {
          if (maskCodes.code66.indexOf(clearValue.substring(1, 7)) >= 0) {
            newMask = '(###-###) ##-##'
            reInit = reInit8 = true
          }
        }
      }

      if (len >= 5 + startDigit && !reInit) {
        if (
          maskCodes.code75.indexOf(
            clearValue.substring(startDigit, 5 + startDigit)
          ) >= 0
        ) {
          newMask = '(###-####) #-##'
          reInit = true
        }
        if (startDigit == 0 && start8 && len >= 6 && !reInit) {
          if (maskCodes.code75.indexOf(clearValue.substring(1, 6)) >= 0) {
            newMask = '(###-####) #-##'
            reInit = reInit8 = true
          }
        }
      }

      if (len >= 5 + startDigit && !reInit) {
        if (
          maskCodes.code65.indexOf(
            clearValue.substring(startDigit, 5 + startDigit)
          ) >= 0
        ) {
          newMask = '(###-###) ##-##'
          reInit = true
        }
        if (startDigit == 0 && start8 && len >= 6 && !reInit) {
          if (maskCodes.code65.indexOf(clearValue.substring(1, 6)) >= 0) {
            newMask = '(###-###) ##-##'
            reInit = reInit8 = true
          }
        }
      }

      if (len >= 5 + startDigit && !reInit) {
        if (
          maskCodes.code55.indexOf(
            clearValue.substring(startDigit, 5 + startDigit)
          ) >= 0
        ) {
          newMask = '(#####) #-##-##'
          reInit = true
        }
        if (startDigit == 0 && start8 && len >= 6 && !reInit) {
          if (maskCodes.code55.indexOf(clearValue.substring(1, 6)) >= 0) {
            newMask = '(#####) #-##-##'
            reInit = reInit8 = true
          }
        }
      }

      if (len >= 4 + startDigit && !reInit) {
        if (
          maskCodes.code54.indexOf(
            clearValue.substring(startDigit, 4 + startDigit)
          ) >= 0
        ) {
          newMask = '(#####) #-##-##'
          reInit = true
        }
        if (startDigit == 0 && start8 && len >= 5 && !reInit) {
          if (maskCodes.code54.indexOf(clearValue.substring(1, 5)) >= 0) {
            newMask = '(#####) #-##-##'
            reInit = reInit8 = true
          }
        }
      }

      if (len >= 4 + startDigit && !reInit) {
        if (startDigit == 0 && start8 && len >= 5) {
          // 4822 || 8482
          if (
            maskCodes.code44ext.indexOf(clearValue.substring(1, 4)) <
            maskCodes.code44ext.indexOf(clearValue.substring(0, 4))
          ) {
            newMask = '(####) ##-##-##'
            reInit = reInit8 = true
          }
        }

        if (
          maskCodes.code44.indexOf(
            clearValue.substring(startDigit, 4 + startDigit)
          ) >= 0
        ) {
          newMask = '(####) ##-##-##'
          reInit = true
        }
        if (startDigit == 0 && start8 && len >= 5 && !reInit) {
          if (maskCodes.code44.indexOf(clearValue.substring(1, 5)) >= 0) {
            newMask = '(####) ##-##-##'
            reInit = reInit8 = true
          }
        }
      }

      if (len >= 3 + startDigit && !reInit) {
        if (
          maskCodes.code33.indexOf(
            clearValue.substring(startDigit, 3 + startDigit)
          ) >= 0
        ) {
          newMask = '(###) ###-##-##'
          reInit = true
        }
        if (startDigit == 0 && start8 && len >= 4 && !reInit) {
          if (maskCodes.code33.indexOf(clearValue.substring(1, 4)) >= 0) {
            newMask = '(###) ###-##-##'
            reInit = reInit8 = true
          }
        }
      }

      if (len >= 3 + startDigit && !reInit) {
        if (
          maskCodes.code53.indexOf(
            clearValue.substring(startDigit, 3 + startDigit)
          ) >= 0
        ) {
          newMask = '(#####) #-##-##'
          reInit = true
        }
        if (startDigit == 0 && start8 && len >= 4 && !reInit) {
          if (maskCodes.code53.indexOf(clearValue.substring(1, 4)) >= 0) {
            newMask = '(#####) #-##-##'
            reInit = reInit8 = true
          }
        }
      }

      if (len >= 2 + startDigit && !reInit) {
        if (
          maskCodes.code52.indexOf(
            clearValue.substring(startDigit, 2 + startDigit)
          ) >= 0
        ) {
          newMask = '(#####) #-##-##'
          reInit = true
        }
        if (startDigit == 0 && start8 && len >= 3 && !reInit) {
          if (maskCodes.code52.indexOf(clearValue.substring(1, 3)) >= 0) {
            newMask = '(#####) #-##-##'
            reInit = reInit8 = true
          }
        }
      }

      if (len > startDigit && !reInit) {
        if (clearValue.substring(startDigit, 1 + startDigit) == 9) {
          newMask = '(###) ###-##-##'
        } else {
          newMask = '(####) ##-##-##'
        }
      }

      if ((startDigit == 1 || reInit8) && len >= 2 && newMask != null) {
        if (clearValue.substring(0, 1) == 7) {
          newMask = '+# ' + newMask
        } else {
          newMask = '# ' + newMask
        }
      } else if (newMask != null && newMask.substring(1) == '#') {
        newMask = newMask.substring(2, newMask.length)
      } else if (newMask != null && newMask.substring(1) == '+') {
        newMask = newMask.substring(3, newMask.length)
      }

      if (newMask != null && newMask != this.phoneMask) {
        this.phoneMask = newMask
      }
    },
  },
}
