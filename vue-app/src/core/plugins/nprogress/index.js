import NProgress from 'nprogress'
import './nprogress.css'

NProgress.configure({
  showSpinner: false,
  easing: 'ease',
  speed: 500,
})

export default (vue) => {
  Object.defineProperty(vue.prototype, '$np', { value: NProgress })
}
