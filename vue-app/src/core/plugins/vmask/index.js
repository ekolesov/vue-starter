import VueMask from 'v-mask'

export default (vue) => {
  vue.use(VueMask)
}
