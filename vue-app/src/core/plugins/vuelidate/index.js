import Vuelidate from 'vuelidate'

export default (vue) => {
  vue.use(Vuelidate)
}
