import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes.js'

Vue.use(Router)

export function createRouter() {
  let router = new Router({
    mode: 'history',
    routes: routes,
    scrollBehavior: () => {
      return { x: 0, y: 0 }
    },
  })

  return router
}
