import Home from 'views/Home.vue'
import NotFound from 'views/errors/NotFound.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/ui',
    name: 'ui',
    component: () => import(/* webpackChunkName: "ui" */ 'views/UI'),
  },
  {
    path: '/doc/:key([\\w\\-_\\/]+[^\\/])?',
    name: 'doc',
    component: () => import(/* webpackChunkName: "doc" */ 'views/Doc'),
  },

  // Errors
  {
    path: '*',
    name: 'notFound',
    component: NotFound,
  },
]

export default routes
