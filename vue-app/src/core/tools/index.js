export default (vue, store) => {
  const tools = {
    stamp: null, // Last activity date

    const: {
      guidEmpty: '00000000-0000-0000-0000-000000000000',
      mainUrl:
        (process.env.SSL_MODE === 'on' ? 'https://' : 'http://') +
        process.env.PUB_HOST +
        (process.env.PUB_PORT ? ':' + process.env.PUB_PORT : ''),
    },

    async connectStoreModule(namespace, storeModule) {
      if (!store.state[namespace]) {
        store.registerModule(namespace, storeModule)
      }
    },

    utils: {
      getHash: length => {
        var result = ''
        var characters =
          'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
        var charactersLength = characters.length
        for (var i = 0; i < length; i++) {
          result += characters.charAt(
            Math.floor(Math.random() * charactersLength)
          )
        }
        return result
      },
    },
  }

  Object.defineProperty(vue.prototype, '$tools', { value: tools })
}
