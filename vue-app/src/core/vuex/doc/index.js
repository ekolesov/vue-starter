import Vue from 'vue'

export default () => {
  const docStore = {
    namespaced: true,

    state: {
      doc: null,
    },

    getters: {
      doc: (state) => state.doc,
    },

    mutations: {
      FETCH_DOC: (state, payload) => {
        state.doc = payload
      },
    },

    actions: {
      clearDoc: async ({ commit }) => {
        commit('FETCH_DOC', null)
      },

      fetchDoc: async ({ commit }, { key }) => {
        return Vue.axios
          .get('/doc', {
            params: {
              key: key,
            },
          })
          .then((response) => {
            commit('FETCH_DOC', response.data)
          })
      },
    },
  }
  return docStore
}
