import Vue from 'vue'
import Vuex from 'vuex'

import DocStore from './doc'

Vue.use(Vuex)

export function createStore() {
  const store = new Vuex.Store({
    modules: {
      docStore: DocStore(),
    },
  })
  return store
}
