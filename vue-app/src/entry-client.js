import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Vue from 'vue'

import { createApp } from './main'
const { app, router, store } = createApp()

import nprogress from '@/core/plugins/nprogress'
nprogress(Vue)

let needAsync = true

if (window.__INITIAL_STATE__) {
  store.replaceState(window.__INITIAL_STATE__)
  needAsync = false
}

router.beforeEach((to, from, next) => {
  // Reload
  if (app.$tools.stamp) {
    let nowDate = new Date()

    // max age - 1 hour
    if (nowDate - app.$tools.stamp > 3600 * 1000) {
      window.location = to.fullPath
    }
  }

  app.$np.start()
  next()
})

router.afterEach(() => {
  app.$np.done()
  // Refresh stamp date
  app.$tools.stamp = new Date()
})

router.onReady(() => {
  app.$mount('#app')
  needAsync = true
})

router.beforeResolve((to, from, next) => {
  const matched = router.getMatchedComponents(to)
  const prevMatched = router.getMatchedComponents(from)

  // compare two list of components from previous route and current route
  let diffed = false
  const activated = matched.filter((c, i) => {
    return diffed || (diffed = prevMatched[i] !== c)
  })

  // if no new components loaded
  if (!activated.length) {
    const forAsync = matched.filter((c) => {
      return c.asyncData
    })
    // for each components for update, asynchorously load data to them
    if (needAsync && forAsync.length) {
      Promise.all(
        forAsync.map((c) => {
          return c.asyncData({ app, store, route: to })
        })
      ).then(
        () => {
          next()
        },
        () => {
          next({ name: 'notFound', params: { '0': to.path } })
          app.$np.done()
        }
      )
    } else {
      return next()
    }
  } else {
    // for each newly loaded components, asynchorously load data to them
    Promise.all(
      activated.map((c) => {
        if (needAsync && c.asyncData) {
          return c.asyncData({ app, store, route: to })
        }
      })
    ).then(
      () => {
        next()
      },
      () => {
        next({ name: 'notFound', params: { '0': to.path } })
        app.$np.done()
      }
    )
  }
})
