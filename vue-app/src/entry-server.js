import { createApp } from './main'

export default (context) => {
  return new Promise((resolve, reject) => {
    const { app, router, store } = createApp()

    const meta = app.$meta()
    router.push(context.url)
    context.meta = meta

    router.onReady(
      () => {
        const matchedComponents = router.getMatchedComponents()

        if (!matchedComponents.length) {
          return reject({ code: 404 })
        } else {
          if (
            matchedComponents.filter((Component) => {
              return Component.name === 'notFound'
            }).length > 0
          ) {
            return reject({ code: 404 })
          }
        }

        var matchComponentsPromise = matchedComponents.map((Component) => {
          if (Component.asyncData) {
            return Component.asyncData({
              app,
              store,
              route: router.currentRoute,
            })
          }
        })

        Promise.all(matchComponentsPromise)
          .then(() => {
            context.state = store.state
            context.code = 200
            return resolve(app)
          })
          .catch(() => {
            return reject({ code: 404 })
          })
      },
      () => {
        return reject({ code: 400 })
      }
    )
  })
}
