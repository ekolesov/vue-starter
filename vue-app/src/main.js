import Vue from 'vue'
import App from './App.vue'

import { createStore } from '@/core/vuex'
import { createRouter } from '@/core/router'

import '@/stylus/_core/resources.css'
import '@/stylus/main.styl'

import '@/core/plugins/meta'

import axios from '@/core/axios'
axios(Vue)

import tools from '@/core/tools'
tools(Vue)

import vuelidate from '@/core/plugins/vuelidate'
vuelidate(Vue)

import vmask from '@/core/plugins/vmask'
vmask(Vue)

// Global register base components
import initUI from '@/core/ui'
initUI(Vue)

// Global mixins
import MetaMixin from '@/core/mixins/meta-mixin.js'
Vue.mixin(MetaMixin)

export function createApp() {
  const router = createRouter()
  const store = createStore()

  const app = new Vue({
    router,
    store,
    ...App,
  })

  return { app, router, store }
}
