const webpack = require('webpack')
const path = require('path')
const fs = require('fs')
const VueSSRServerPlugin = require('vue-server-renderer/server-plugin')
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin')
const nodeExternals = require('webpack-node-externals')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin

// need .env file (local.env as example)
if (!fs.existsSync('./.env')) {
  throw new Error('need .env file')
}

const Dotenv = require('dotenv-webpack')
const dotenvPlugin = new Dotenv()

let pluginsForClientEntry = [
  new VueSSRClientPlugin(),
  new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
]

if (process.env.ANALYZER_MODE === 'on') {
  pluginsForClientEntry.push(new BundleAnalyzerPlugin())
}

let pluginsForServerEntry = [new VueSSRServerPlugin()]

const TARGET_NODE = process.env.WEBPACK_TARGET === 'node'

module.exports = {
  outputDir: TARGET_NODE
    ? path.resolve(__dirname, './parts/bundle')
    : path.resolve(__dirname, './wwwroot'),
  assetsDir: 'assets',
  productionSourceMap: false,
  // lintOnSave: process.env.NODE_ENV !== 'production',
  configureWebpack: {
    devtool: '',
    entry: TARGET_NODE
      ? {
          ssr: './src/entry-server',
        }
      : {
          app: './src/entry-client',
        },
    target: TARGET_NODE ? 'node' : 'web',
    node: TARGET_NODE ? undefined : false,
    plugins: [
      ...(TARGET_NODE ? pluginsForServerEntry : pluginsForClientEntry),
      dotenvPlugin,
    ],
    externals: TARGET_NODE
      ? nodeExternals({
          whitelist: /\.css$/,
        })
      : undefined,
    output: TARGET_NODE
      ? {
          libraryTarget: 'commonjs2',
        }
      : {
          filename: 'assets/js/[name].[hash].js',
          chunkFilename: 'assets/js/[name].[hash].js',
          globalObject: 'this',
          libraryTarget: undefined,
        },
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 250000,
        cacheGroups: {
          vendors: {
            name: 'chunk-vendors', // eslint-disable-next-line
            test: /[\\\/]node_modules[\\\/]/,
            priority: -10,
            chunks: 'all',
          },
        },
      },
    },
    resolve: {
      alias: {
        components: path.resolve(__dirname, './src/components'),
        views: path.resolve(__dirname, './src/views'),
        '@': path.resolve(__dirname, './src'),
      },
    },
  },
  css: {
    extract: true,
    sourceMap: false,
  },
  chainWebpack: (config) => {
    if (TARGET_NODE || process.env.NODE_ENV === 'production') {
      config.entryPoints.delete('app')
    }
    if (
      process.env.SSR_MODE === 'on' ||
      TARGET_NODE ||
      process.env.NODE_ENV === 'production'
    ) {
      config.plugins.delete('hmr')
      config.plugins.delete('preload')
      config.plugins.delete('prefetch')
      config.plugins.delete('html')
    }
    if (process.env.SSR_MODE === 'off') {
      config.plugin('html').tap((args) => {
        args[0].template = 'parts/templates/client.template.html'
        args[0].filename = 'index.html'
        return args
      })
    }

    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule.use('vue-svg-loader').loader('vue-svg-loader')
  },

  devServer: {
    host: process.env.PUB_HOST,
    stats: 'errors-only',
    https:
      process.env.SSL_MODE === 'on'
        ? {
            key: fs.readFileSync(
              path.resolve(__dirname, './parts/ssl/localhost.pem')
            ),
            cert: fs.readFileSync(
              path.resolve(__dirname, './parts/ssl/localhost.crt')
            ),
          }
        : undefined,
    hot: true,
    contentBase: path.resolve(__dirname, './wwwroot'),
    watchContentBase: true,
    // compress: true,
    port: process.env.PUB_PORT,
  },
}
